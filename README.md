# op_cid

`op_cid` is an Anagolay Operation that generates an IPFS Content Identifier (CID) from a given `op_multihash::U64MultihashWrapper` input,
following [Version 1 (v1)](https://docs.ipfs.io/concepts/content-addressing/#version-1-v1]) specification.  

It provides a `String` output, according to its manifest data:

```
{
  "name": "op_cid",
  "desc": "Anagolay CID operation. Generates the V1 of the CID",
  "input": [
      "op_multihash::U64MultihashWrapper",
  ],
  "config": {},
  "groups": [
    "SYS"
  ],
  "output": "String",
  "repository": "https://gitlab.com/anagolay/operations/op_cid",
  "license": "Apache 2.0",
  "features": [
    "std"
  ],
}
```

## Installation

In the Cargo.toml file of your Workflow, specify a dependency toward **op_cid** crate:

```toml
[dependencies]
op_cid = { git = "https://gitlab.com/anagolay/operation/op_cid" }
```

This operation depends on `op_multihash`:

```toml
op_multihash = { version = "0.1.0", default-features = false, features = [
"anagolay",
], git = 'https://gitlab.com/anagolay/operations/op_multihash' }
```

The `op_multihash` git repository must be the one indicated in its manifest data, because 
it's going to be patched upon Workflow build. Not following this convention will result in
Workflow build failure due to the presence of different versions for the same crate.

This crate uses IPFS dependencies. In order for the git fetch to work, `cargo` must fetch via system git instead of the
local, with the option [netgit-fetch-with-cli](https://doc.rust-lang.org/cargo/reference/config.html#netgit-fetch-with-cli)
This configuration is stored in `.cargo/config.toml`

## Features

* **anagolay** Used to build this crate as dependency for other operations (default)
* **js** Used to build the javascript bindings (default)
* **std** Enable preferential compilation for std environment (default) 
* **debug_assertions** Used to produce helpful debug messages (default), requires **std**
* **test** Used to build and execute tests, requires **std**

## Usage

### Execution

From Rust:

```rust
use op_cid;

let input = b"example".to_vec();
let config = std::collections::BTreeMap::new();
let output = op_cid::execute(input, config).await.unwrap();
```
Or in WASM environment:

```javascript
import { execute } from "@anagolay/op_cid"

async function main() {
  const input = "example";
  const config = {}
  let output = await execute([input], config)
}
```

## Development

It is suggested to use the Vscode `.devcontainer` or any other sandboxed environment like Gitpod because the Operations must be developed under the same environment as they are published.

Git hooks will be initialized on first build or a test. If you wish to init them before run this in your terminal:

```shell
rusty-hook init
```

To help you with writing consistent code we provide you with following:

-   `.gitlab-ci.yml` for your testing and fmt checks
-   `rusty-hook.toml` for the `pre-push` fmt check so your CI is green.

### Manifest generation

```shell
makers manifest
```

## License

[![License](https://img.shields.io/badge/License-Apache_2.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

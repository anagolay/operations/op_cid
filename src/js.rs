use crate::execute;
#[cfg(not(feature = "std"))]
use alloc::{
    string::{String, ToString},
    vec::Vec,
};
use an_operation_support::{from_map, from_value, to_value};
use js_sys::Map;
use op_multihash::U64MultihashWrapper;
#[cfg(debug_assertions)]
use std::panic;
use wasm_bindgen::prelude::wasm_bindgen;
use wasm_bindgen::JsValue;

/// op_cid
///
/// Compute the multibase encoded Content IDentifier of the
/// [`op_multihash::U64MultihashWrapper`] provided in input.
/// This is the WASM binding of the Operation `execute()` function
///
/// # Arguments
///  * operation_inputs: collection of serialized inputs
///  * config: configuration map
///
/// # Return
/// A promise that resolves to a [`String`] if it succeeds, or to an error message
/// [`String`] otherwise
#[wasm_bindgen(js_name=execute)]
pub async fn wasm_execute(operation_inputs: Vec<JsValue>, config: Map) -> Result<JsValue, JsValue> {
    #[cfg(debug_assertions)]
    panic::set_hook(Box::new(console_error_panic_hook::hook));

    let input: U64MultihashWrapper = from_value(operation_inputs.get(0).unwrap())?;
    let config = from_map(&config.into())?;

    let output = execute(&input, config).await?;

    to_value(&output)
}

/// Output manifest
#[wasm_bindgen(js_name=describe)]
pub fn wasm_describe() -> String {
    #[cfg(debug_assertions)]
    panic::set_hook(Box::new(console_error_panic_hook::hook));
    crate::describe()
}
